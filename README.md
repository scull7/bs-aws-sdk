[![pipeline status](https://gitlab.com/scull7/bs-aws-sdk/badges/master/pipeline.svg)](https://gitlab.com/scull7/bs-aws-sdk/commits/master)
[![coverage report](https://gitlab.com/scull7/bs-aws-sdk/badges/master/coverage.svg)](https://scull7.gitlab.io/bs-aws-sdk/coverage)

# Build
```
npm run build
```

# Watch

```
npm run watch
```


# Editor
If you use `vscode`, Press `Windows + Shift + B` it will build automatically
