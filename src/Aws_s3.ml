module Acl = struct
  type t =
    | Private
    | PublicRead
    | PublicReadWrite
    | AuthenticatedRead
    | AwsExecRead
    | BucketOwnerRead
    | BucketOwnerFullControl

  let to_string = function
    | Private -> "private"
    | PublicRead -> "public-read"
    | PublicReadWrite -> "public-read-write"
    | AuthenticatedRead -> "authenticated-read"
    | AwsExecRead -> "aws-exec-read"
    | BucketOwnerRead -> "bucket-owner-read"
    | BucketOwnerFullControl -> "bucket-owner-full-control"
end

module Body = struct
  (*
   * @TODO - Implement ReadableStream
   * @TODO - Implement TypedArray
   * @TDOO - Implement Blob
   *)
  type t =
    | Buffer of Node.buffer
    | String of string
end

module StorageClass = struct
  type t =
    | Standard
    | ReducedRedundancy
    | StandardIA
    | OneZoneIA

  let to_string = function
    | Standard -> "STANDARD"
    | ReducedRedundancy -> "REDUCED_REDUNDANCY"
    | StandardIA -> "STANDARD_IA"
    | OneZoneIA -> "ONEZONE_IA"
end

module Client = struct
  type t

  module Options = struct
    type t = {
      version: string [@bs.as "apiVersion"][@bs.optional];
      access_key_id: string [@bs.as "accessKeyId"][@bs.optional];
      secret_access_key: string [@bs.as "secretAccessKey"][@bs.optional];
      region: string [@bs.optional];
    } [@@bs.deriving abstract ]
  end

  external s3 : Options.t -> t = "aws-sdk/clients/s3" [@@bs.new] [@@bs.module]

  let make ?(version="latest") ~access_key_id ~secret_access_key ~region _ =
    let options = Options.t
      ~version
      ~access_key_id
      ~secret_access_key
      ~region:(region |. Aws_region.to_string)
      ()
    in
    s3 options

end

module Object = struct
  module Head = struct
    module Response = struct
      type t = {
        marker: bool [@bs.as "DeleteMarker"][@bs.optional];
        accept_ranges: string [@bs.as "AcceptRanges"];
        disposition: string [@bs.as "ContentDisposition"][@bs.optional];
        encoding: string [@bs.as "ContentEncoding"][@bs.optional];
        etag: string [@bs.as "ETag"];
        expires: Js.Date.t [@bs.as "Expires"][@bs.optional];
        language: string [@bs.as "ContentLanguage"][@bs.optional];
        length: int [@bs.as "ContentLength"];
        metadata: string Js.Dict.t [@bs.as "Metadata"];
        modified: Js.Date.t [@bs.as "LastModified"];
        content_type: string [@bs.as "ContentType"];
        version: string [@bs.as "VersionId"];

        cache_control: string [@bs.as "CacheControl"][@bs.optional];
        parts_count: int [@bs.as "PartsCount"];
        expiration: string [@bs.as "Expiration"][@bs.optional];
        missing_meta: int [@bs.as "MissingMeta"];
        request_charged: string [@bs.as "RequestCharged"][@bs.optional];
        restore: string [@bs.as "Restore"][@bs.optional];
        replication_status: string [@bs.as "ReplicationStatus"];
        server_side_encryption:
          string [@bs.as "ServerSideEncryption"][@bs.optional];
        sse_customer_algo: string [@bs.as "SSECustomerAlgorithm"][@bs.optional];
        sse_customer_key: string [@bs.as "SSECustomerKeyMD5"][@bs.optional];
        sse_kms_key_id: string [@bs.as "SSEKMSKeyId"][@bs.optional];
        storage_class:string [@bs.as "StorageClass"][@bs.optional];
        website_redirect:
          string [@bs.as "WebsiteRedirectlocation"][@bs.optional];
      } [@@bs.deriving abstract]
    end

    module Params = struct
      type t = {
        bucket: string [@bs.as "Bucket"];
        key: string [@bs.as "Key"];
        version: string [@bs.as "VersionId"][@bs.optional];
        if_match: string [@bs.as "IfMatch"][@bs.optional];
        if_modified_since: Js.Date.t [@bs.as "IfModidfiedSince"][@bs.optional];
        if_none_match: string [@bs.as "IfNoneMatch"][@bs.optional];
        if_unmodified_since:
          Js.Date.t [@bs.as "IfUnmodifiedSince"][@bs.optional];
        part_number: int [@bs.as "PartNumber"][@bs.optional];
        range: string [@bs.as "Range"][@bs.optional];
        sse_customer_algo:
          string [@bs.as "SSECustomerAlgorithm"][@bs.optional];
        sse_customer_key: string [@bs.as "SSECustomerKey"][@bs.optional];
        sse_customer_key_md5:
          string [@bs.as "SSECustomerKeyMD5"][@bs.optional];
        request_payer: string [@bs.as "RequestPayer"][@bs.optional];
      } [@@bs.deriving abstract]

      let make = t
        
    end

    external head_object :
      Client.t ->
      Params.t ->
      (Aws_exn.Error.t Js.Nullable.t -> Response.t Js.Nullable.t -> unit) ->
      unit =
      "headObject" [@@bs.send]

    let run
      ~bucket
      ~key
      ?if_match
      ?if_modified_since
      ?if_none_match
      ?if_unmodified_since
      ?part_number
      ?range
      ?version
      ?sse_customer_algo
      ?sse_customer_key
      ?sse_customer_key_md5
      ?request_payer
      client =
        let params = Params.make
          ~bucket
          ~key
          ?if_match
          ?if_modified_since
          ?if_none_match
          ?if_unmodified_since
          ?part_number
          ?range
          ?version
          ?sse_customer_algo
          ?sse_customer_key
          ?sse_customer_key_md5
          ?request_payer
          ()
        in
        Js.Promise.make (fun ~resolve ~reject ->
          head_object client params (fun err data ->
            match (err |. Js.Nullable.toOption) with
            | Some(exn) -> reject (exn |. Aws_exn.to_exn) [@bs]
            | None ->
              resolve (data |. Js.Nullable.toOption |. Belt.Option.getExn) [@bs]
          )
        )

  end
  module Put = struct

    module Response = struct
      type t = {
        expiration: string [@bs.as "Expiration"][@bs.optional];
        etag: string [@bs.as "ETag"];
        server_side_encryption:
          string [@bs.as "ServerSideEncryption"][@bs.optional];
        version: string [@bs.as "VersionId"];
        sse_customer_algo: string [@bs.as "SSECustomerAlgorithm"][@bs.optional];
        sse_customer_key: string [@bs.as "SSECustomerKeyMD5"][@bs.optional];
        sse_kms_key_id: string [@bs.as "SSEKMSKeyId"][@bs.optional];
        request_charged: string [@bs.as "RequestCharged"][@bs.optional];
      } [@@bs.deriving abstract]

      
    end

    module Params = struct
      type content

      type t = {
        body: content [@bs.as "Body"];
        bucket: string [@bs.as "Bucket"];
        key: string [@bs.as "Key"];
        content_type: string [@bs.as "ContentType"];
        acl: string [@bs.as "ACL"][@bs.optional];
        storage_class:string [@bs.as "StorageClass"][@bs.optional];
      } [@@bs.deriving abstract]

      external to_content : 'a -> content = "%identity"

      let content_of_body = function
        | Body.Buffer b -> to_content b
        | Body.String s -> to_content s

      let make ~body ~bucket ~key ~content_type?acl ?storage_class _ =
        let acl =
          acl
          |. Belt.Option.map Acl.to_string
        in
        let storage_class =
          storage_class
          |. (Belt.Option.map StorageClass.to_string)
        in
        t
          ~body: (content_of_body body)
          ~bucket
          ~key
          ~content_type
          ?acl
          ?storage_class
          ()
    end

    external put_object :
      Client.t ->
      Params.t -> 
      (Aws_exn.Error.t Js.Nullable.t -> Response.t Js.Nullable.t -> unit) ->
      unit =
      "putObject" [@@bs.send]

    let run ~body ~bucket ~key ~content_type ?acl ?storage_class client =
      let params = Params.make ~body ~bucket ~key ~content_type ?acl ?storage_class ()
      in
      Js.Promise.make (fun ~resolve ~reject ->
        put_object client params (fun err data ->
          match (err |. Js.Nullable.toOption) with
          | Some(exn) -> reject (exn |. Aws_exn.to_exn) [@bs]
          | None ->
            resolve (data |. Js.Nullable.toOption |. Belt.Option.getExn) [@bs]
        )
      )
  end

  module Delete = struct

    module Response = struct
      type t = {
        marker: bool [@bs.as "DeleteMarker"][@bs.optional];
        version: string [@bs.as "VersionId"];
        request_charged: string [@bs.as "RequestCharged"][@bs.optional];
      } [@@bs.deriving abstract]
    end
    
    module Params = struct
      type t = {
        bucket: string [@bs.as "Bucket"];
        key: string [@bs.as "Key"];
        mfa: string [@bs.as "MFA"][@bs.optional];
        version: string [@bs.as "VersionId"];
        request_payer: string [@bs.as "RequestPayer"][@bs.optional];
      } [@@bs.deriving abstract]

      let make = t
    end

    external delete_object :
      Client.t ->
      Params.t ->
      (Aws_exn.Error.t Js.Nullable.t -> Response.t Js.Nullable.t -> unit) ->
      unit =
      "deleteObject" [@@bs.send]

    let run ~bucket ~key ?mfa ~version ?request_payer client =
      let params = Params.make ~bucket ~key ?mfa ~version ?request_payer ()
      in
      Js.Promise.make (fun ~resolve ~reject ->
        delete_object client params (fun err data ->
          match (err |. Js.Nullable.toOption) with
          | Some(exn) -> reject (exn |. Aws_exn.to_exn) [@bs]
          | None ->
            resolve (data |. Js.Nullable.toOption |. Belt.Option.getExn) [@bs]
        )
      )
  end
end
