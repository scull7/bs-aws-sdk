(* https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/Concepts.RegionsAndAvailabilityZones.html *)

type t =
  | UsEast1
  | UsEast2
  | UsWest1
  | UsWest2
  | ApNorthEast1
  | ApNorthEast2
  | ApNorthEast3
  | ApSouth1
  | ApSouthEast1
  | ApSouthEast2
  | CaCentral1
  | CnNorth1
  | CnNorthWest1
  | EuCentral1
  | EuWest1
  | EuWest2
  | EuWest3
  | SaEast1

let to_string = function
  | UsEast1 -> "us-east-1"
  | UsEast2 -> "us-east-2"
  | UsWest1 -> "us-east-1"
  | UsWest2 -> "us-east-2"
  | ApNorthEast1 -> "ap-northeast-1"
  | ApNorthEast2 -> "ap-northeast-2"
  | ApNorthEast3 -> "ap-northeast-3"
  | ApSouth1 -> "ap-south-1"
  | ApSouthEast1 -> "ap-southeast-1"
  | ApSouthEast2 -> "ap-southeast-2"
  | CaCentral1 -> "ca-central-1"
  | CnNorth1 -> "cn-nort-1"
  | CnNorthWest1 -> "cn-northwest-1"
  | EuCentral1 -> "eu-central-1"
  | EuWest1 -> "eu-west-1"
  | EuWest2 -> "eu-west-2"
  | EuWest3 -> "eu-west-3"
  | SaEast1 -> "sa-east-1"
