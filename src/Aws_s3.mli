module Acl : sig
  type t =
    | Private
    | PublicRead
    | PublicReadWrite
    | AuthenticatedRead
    | AwsExecRead
    | BucketOwnerRead
    | BucketOwnerFullControl

  val to_string : t -> string
end


module Body : sig
  type t =
    | Buffer of Node.buffer
    | String of string
end


module StorageClass : sig
  type t =
    | Standard
    | ReducedRedundancy
    | StandardIA
    | OneZoneIA

  val to_string : t -> string
end


module Client : sig
  type t

  val make :
    ?version:string ->
    access_key_id:string ->
    secret_access_key:string ->
    region: Aws_region.t ->
    unit ->
    t
end


module Object : sig

  (*
   * ## HEAD Operation
   * The HEAD operation retrieves metadata from an object without returning
   * the object itself.  This operation is useful if you're only interested
   * in an object's metadata.  To use HEAD, you must have READ access to the
   * object
   *
   * [headObject]:https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#headObject-property
   *)
  module Head : sig
    
    module Response : sig
      type t

      (* Specifies whether the object retrieved was (true) or was not (false)
       * a Delete Marker.  If false, this response header does not appear in
       * the response.
       *)
      val markerGet : t -> bool option

      val content_typeGet: t -> string

      val expirationGet : t -> string option

      val modifiedGet : t -> Js.Date.t

      val lengthGet : t -> int

      val etagGet : t -> string

      val expiresGet : t -> Js.Date.t option

      val versionGet : t -> string
    end

    val run :
      bucket:string ->
      key:string ->
      ?if_match:string ->
      ?if_modified_since:Js.Date.t ->
      ?if_none_match:string ->
      ?if_unmodified_since:Js.Date.t ->
      ?part_number:int ->
      ?range: string ->
      ?version: string ->
      ?sse_customer_algo: string ->
      ?sse_customer_key: string ->
      ?sse_customer_key_md5: string ->
      ?request_payer: string ->
      Client.t ->
      Response.t Js.Promise.t
  end

  module Put : sig
    
    module Response : sig
      type t

      val etagGet : t -> string

      val expirationGet : t -> string option

      val versionGet : t -> string
    end

    val run :
      body:Body.t ->
      bucket:string ->
      key:string ->
      content_type:string ->
      ?acl:Acl.t ->
      ?storage_class:StorageClass.t ->
      Client.t ->
      Response.t Js.Promise.t
  end

  module Delete : sig

    module Response : sig
      type t

      val markerGet : t -> bool option

      val request_chargedGet : t -> string option 

      val versionGet : t -> string
    end

    val run :
      bucket:string ->
      key:string ->
      ?mfa:string ->
      version:string ->
      ?request_payer:string ->
      Client.t ->
      Response.t Js.Promise.t

  end
end (* END Object *)
