
module Error = struct
  type t = {
    message: string Js.Nullable.t;
    code: string;
    region: string Js.Nullable.t;
    time: Js.Date.t;
    request_id: string [@bs.as "requestId"];
    extended_request_id: string [@bs.as "extendedRequestId"];
    cf_id: string [@bs.as "cfId"][@bs.optional];
    status: int [@bs.as "statusCode"];
    retryable: bool;
    retry_delay: float [@bs.as "retryDelay"];
  } [@@bs.deriving abstract]

  let messageGet t =
    match (t |. messageGet |. Js.Nullable.toOption) with
    | Some msg -> msg
    | None -> t |. codeGet
end

exception Unknown of string
exception Not_found of string

external from_promise_error : Js.Promise.error -> exn = "%identity"

let to_exn error =
  match (error |. Error.statusGet) with
  | 404 -> error |. Error.messageGet |. Not_found
  | _ -> error |. Error.messageGet |. Unknown

let from_promise_error error = error |. from_promise_error
