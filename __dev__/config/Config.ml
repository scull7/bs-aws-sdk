

let compile _ =
  let basepath = Belt.Option.getExn [%bs.node __dirname] in
  let local_file = Node.Path.resolve basepath "../../config/test-local.toml" in
  
  Nconf.(
    make ()
    |> argv ()
    |> env ()
    |> tomlFilePathNamed "test_local" local_file
  )

let get config key = Nconf.getKey config key |. Js.Nullable.toOption

let getExn config key = config |. get key |. Belt.Option.getExn
