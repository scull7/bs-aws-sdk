open Ava

let dirname = Belt.Option.getExn([%bs.node __dirname]);

external readFileSync : string -> Node.buffer = "" [@@bs.module "fs"]

let config = Config.compile ()
let access_key_id = Config.getExn config "AWS_ACCESS_KEY_ID"
let secret_access_key = Config.getExn config "AWS_SECRET_ACCESS_KEY"
let bucket = "bs-aws"

let () =

Async.test ava "should store and retrieve a file" (fun t ->
  let _ = Test.plan t, 5 in
  let key = "200_tux_svg.png" in
  let path = Node.Path.resolve dirname "../assets/200px-Tux.svg.png" in
  let buffer = readFileSync path in
  let client = Aws_s3.Client.make
    ~access_key_id
    ~secret_access_key
    ~region:Aws_region.UsEast2
    ()
  in
  Aws_s3.Object.Put.run
    ~body:(Aws_s3.Body.Buffer buffer)
    ~bucket
    ~key
    ~content_type:"image/png"
    client
  |> Js.Promise.then_ (fun res ->
      let etag = res |. Aws_s3.Object.Put.Response.etagGet in
      let etag_len = etag |. String.length in
      let version =res |. Aws_s3.Object.Put.Response.versionGet in
      let version_len = version |. String.length in
      let _ = Assert.true_ t (etag_len == 34) in
      let _ = Assert.true_ t (version_len == 32) in

      Aws_s3.Object.Head.run
        ~bucket
        ~key
        client
  )
  |> Js.Promise.then_ (fun res ->
      let content_length = res |. Aws_s3.Object.Head.Response.lengthGet in
      let type_ = res |. Aws_s3.Object.Head.Response.content_typeGet in

      let _ = Assert.true_ t (content_length == 21558) in
      let _ = Assert.true_ t (type_ == "image/png") in

      Aws_s3.Object.Delete.run
        ~bucket
        ~key
        ~version:(Aws_s3.Object.Head.Response.versionGet res)
        client
  )
  |> Js.Promise.then_ (fun res ->
      let version = res |. Aws_s3.Object.Delete.Response.versionGet in
      let version_len = version |. String.length in
      let _ = Assert.true_ t (version_len == 32) in
      Js.Promise.resolve ()
  )
  |> Js.Promise.catch (fun err ->
    let _ = Js.Console.error2 "ERROR: " err in
    let _ = Test.fail t in
    Js.Promise.resolve ()
  )
  |> Js.Promise.then_ (fun _ -> Test.finish t |> Js.Promise.resolve)
  |. ignore
);

Async.test ava "should error when issuing a head request for a object that does not exist" (fun t ->
  let _ = Test.plan t 1 in
  let key = "1000px-Tux.svg.png" in
  let client = Aws_s3.Client.make
    ~access_key_id
    ~secret_access_key
    ~region:Aws_region.UsEast2
    ()
  in
  Aws_s3.Object.Head.run
    ~bucket
    ~key
    client
  |> Js.Promise.then_ (fun _ ->
      let _ = Test.fail ~message:"unexpected_success response" t in
      Js.Promise.resolve ()
  )
  |> Js.Promise.catch (fun err ->
      let _ =
        match (err |. Aws_exn.from_promise_error) with
        | Aws_exn.Not_found _ -> Test.pass t
        | err -> Test.fail ~message:(err |. Js.String.make) t
      in
      Js.Promise.resolve ()
  )
  |> Js.Promise.then_ (fun _ -> Test.finish t |> Js.Promise.resolve)
  |. ignore
)
